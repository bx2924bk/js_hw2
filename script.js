function checking(inputTime) {
    if (inputTime >= 0 && inputTime <= 15){
        return 'first quarter'
    } else if (inputTime > 15 && inputTime <= 30){
        return 'second quarter'
    } else if (inputTime > 30 && inputTime <= 45){
        return 'third quarter'
    } else {
        return 'fourth quarter'
    }
}

let minutes, i = 0;
while (i < 10){
    minutes= Math.floor(Math.random()*60);
    console.log(minutes, checking(minutes));
    i++;
}